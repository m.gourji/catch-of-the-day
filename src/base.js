import Rebase from "re-base";
import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyBTA8iw10a9VY2G1EBJHVXSIHlG-_PjdhI",
    authDomain: "catch-of-the-day-68332.firebaseapp.com",
    databaseURL: "https://catch-of-the-day-68332.firebaseio.com"
});
const base =Rebase.createClass(firebaseApp.database());
// this is a name export
export { firebaseApp };
// this is a default export
export default base;
