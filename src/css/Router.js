import React from 'react';
import { BrowserRouter, Route, Switch  } from 'react-router-dom';
import StorePicker from './components/StorePicker';
import NotFound from './components/NotFound';
import App from './App';

import "./css/style.css";

const Router = () => (
  <BrowserRouter>
  <switch>
  <Route exact path="/" component={StorePicker} /> 
  <Route path="/store/:storeId" component={App} />
  <Route component={NotFound} />
   </switch>


  </BrowserRouter>
);

export default Router;